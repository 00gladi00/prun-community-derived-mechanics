---
title: "HQ"
---

## Upgrade Costs

| From      | To        | Part Costs |     |     |     |     |     |     |     |     |     |     |
|-----------|-----------|------------|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| x Permits | y Permits | BBH        | BSE | BDE | BTA | LBH | LSE | LDE | LTA | MCG | TRU | OFF |
| 2         | 3         | 12         | 8   | 8   | 4   | 0   | 0   | 0   | 0   | 24  | 8   | 0   |
| 3         | 4         | 12         | 8   | 8   | 4   | 0   | 0   | 0   | 0   | 24  | 8   | 0   |
| 4         | 5         | 24         | 16  | 16  | 8   | 0   | 0   | 0   | 0   | 48  | 16  | 0   |
| 5         | 6         | 48         | 32  | 32  | 16  | 0   | 0   | 0   | 0   | 96  | 32  | 0   |
| 6         | 7         | 96         | 64  | 64  | 32  | 0   | 0   | 0   | 0   | 192 | 64  | 0   |
| 7         | 8         | 0          | 0   | 0   | 0   | 14  | 10  | 10  | 6   | 40  | 10  | 15  |
| 8         | 9         | 0          | 0   | 0   | 0   | 25  | 18  | 18  | 11  | 70  | 18  | 26  |
| 9         | 10        | 0          | 0   | 0   | 0   | 43  | 31  | 31  | 18  | 123 | 31  | 46  |
| 10        | 11        | 0          | 0   | 0   | 0   | 75  | 54  | 54  | 32  | 214 | 54  | 80  |
| 11        | 12        | 0          | 0   | 0   | 0   | 131 | 94  | 94  | 56  | 375 | 94  | 141 |
| **Prediction**                                                                                 |
| 12        | 13        | 0          | 0   | 0   | 0   | 230 | 164 | 164 | 98  | 657 | 164 | 246 |


The prediction is based on the follow formula:

`BaseCost * 1.75^n`, where n = "prefab tier-count" and n is 0 starting at the switch from 7 -> 8 (when L-fabs started). The formula will most likely change once it switches to R-tier Fab requirements.

## HQ Bonus

The multiplier to the base efficiency bonus provided by the HQ can be given by:

`Multiplier = -2 * (UsedPermits / TotalPermits) + 3`
